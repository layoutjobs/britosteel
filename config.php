<?php
/* 
 * Configurações do mapa.
 */
$mapaLat			= -22.922439; // Latitude.
$mapaLng			= -47.089516; // Longitude.
$mapaZoom			= 16; // Zoom.

/* 
 * Configurações do formulário de contato.
 */
$emailsContato 		= array('contato@britosteel.com.br', 'lucimara@britosteel.com.br');

$mailIsSMTP     	= false;

/* 
 * Considerados somente se $mailIsSMTP = true
 */
$mailHost       	= 'smtp.gmail.com';
$mailPort       	= 465;
$mailSMTPAuth   	= true;
$mailSMTPSecure 	= 'ssl';
$mailSMTPDebug  	= 0;
$mailUsername   	= '';
$mailPassword   	= '';
$mailCharSet    	= 'utf-8';