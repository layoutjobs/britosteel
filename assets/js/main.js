$(document).ready(function(){
	
	// Define a altura do container igual ao do browser.
	$('body > .container > .container > .container').css('min-height', $(document).height());

	// Banners
	$('#banners.carousel').carousel().bind('slid', function(){
		$(this).find('.active img').fadeOut();
		$(this).find('.active img').fadeIn('100');
	});
	
	// Galeria de imagens
	var slider = $('.gallery.carousel').carousel({ 
		interval: 4000
	}).bind('slid', function(){
		var index = $(this).find('.active').index();
		$(this).find('.carousel-pagination a').removeClass('pagination-active').eq(index).addClass('pagination-active');
	});

	$('.gallery.carousel .carousel-pagination a').click(function(e){
		var index = $(this).index();
		slider.carousel(index);
		e.preventDefault();
	});
});