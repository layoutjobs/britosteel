<?php 
/**
 * Carrega o arquivo de configuração.
 */     
if ($_SERVER['HTTP_HOST'] === 'localhost' || $_SERVER['HTTP_HOST'] === 'localhost:8080')
	include('config.local.php');
else
	include('config.php');

/**
 * Seta as variáveis.
 */     
$siteTitle 		= 'Brito Steel';
$path 			= 'http://';
$path			.= str_replace('www.', '', $_SERVER['HTTP_HOST']);
$path 			.= str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
$folder			= $_SERVER['DOCUMENT_ROOT'];
$folder			.= str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);  
$cssPath 		= $path . '/assets/css/';
$imgPath 		= $path . '/assets/img/';
$jsPath 		= $path . '/assets/js/';
$pluginsPath 	= $path . '/assets/plugins/';
$uploadsPath 	= $path . '/uploads/';
  
/**
 * Trata os GETs.
 */      
$get = isset($_GET['p']) ? $_GET['p'] : '';
$getArr = explode('/', $get);
$getPage = isset($getArr[0]) ? $getArr[0] : 'home';
$getSubPage = isset($getArr[1]) ? $getArr[1] : '';

/**
 * Definição de todas as páginas e respectivos atributos.
 * Os possíveis atributos de cada página são o seguinte:
 * alias: o nome da página, deve coincidir com o GET.
 */     
$pages = array(
	array(
		'alias'=>'home',
		'title'=>'Home',
	),
	array(
		'alias'=>'quem-somos',
		'title'=>'Quem Somos',
		'banners'=>array('2', '3', '4', '1'),
	),
	array(
		'alias'=>'servicos',
		'title'=>'Serviços',
		'plugins'=>array('prettyphoto'),
		'banners'=>array('3', '4', '1', '2'),
	),	
	array(
		'alias'=>'qualidade',
		'title'=>'Qualidade',
		'banners'=>array('4', '1', '2', '3'),
	),
	array(
		'alias'=>'dados-cadastrais',
		'title'=>'Dados Cadastrais',
		'fileRender'=>$uploadsPath . 'cadastro_britosteel.pdf',
	),
	array(
		'alias'=>'localizacao',
		'title'=>'Localização',
		'banners'=>array('2', '3', '4', '1'),
	),
	array(
		'alias'=>'contato',
		'title'=>'Contato',
		'banners'=>array('3', '4', '1', '2'),
	),
	array(
		'alias'=>'clientes',
		'title'=>'Clientes',
		'banners'=>array('4', '1', '2', '3'),
	),
);
	
/**
 * Instancia um objeto padrão com os atributos da página a ser exibida
 * de acordo com o GET. O padrão (sem GET) é a página home.
 */     
$Page = new stdClass;
$Page->parent = '';
$Page->alias = 'home';
$Page->title = 'Bem-vindo';	
$Page->fileRender = '';	
$Page->banners = array('1', '2', '3', '4');	
$Page->plugins = array();	

foreach ($pages as $page) {
	if ($page['alias'] === $getPage) {
		if ((isset($page['defaultItem']) && ($getSubPage === ''))) {
			foreach ($page['items'] as $subPage) {
				if ($subPage['alias'] === $page['defaultItem']) {
					$Page->parent = $page['alias'];
					$Page->alias = $subPage['alias'];
					$Page->title = $subPage['title'];
					$Page->fileRender = isset($subPage['fileRender']) ? $subPage['fileRender'] : $Page->fileRender;
					$Page->banners = isset($subPage['banners']) ? $subPage['banners'] : $Page->banners;
					$Page->plugins = isset($subPage['plugins']) ? $subPage['plugins'] : $Page->plugins;
				}
			}			
		} else {
			if ($getSubPage === '') {
				$Page->alias = $page['alias'];
				$Page->title = $page['title'];
				$Page->fileRender = isset($page['fileRender']) ? $page['fileRender'] : $Page->fileRender;
				$Page->banners = isset($page['banners']) ? $page['banners'] : $Page->banners;
				$Page->plugins = isset($page['plugins']) ? $page['plugins'] : $Page->plugins;
			} else {
				foreach ($page['items'] as $subPage) {
					if ($subPage['alias'] === $getSubPage) {
						$Page->parent = $page['alias'];
						$Page->alias = $subPage['alias'];						
						$Page->title = $subPage['title'];
						$Page->fileRender = isset($subPage['fileRender']) ? $subPage['fileRender'] : $Page->fileRender;
						$Page->banners = isset($subPage['banners']) ? $subPage['banners'] : $Page->banners;
						$Page->plugins = isset($subPage['plugins']) ? $subPage['plugins'] : $Page->plugins;
					}
				}				
			}
		}
	}
}

/**
 * Definição dos itens do menu pricipal (topo).
 */     		    
$menu = '<ul class="nav">';
foreach ($pages as $page) {
  	if ($page['alias'] === 'home') $page['alias'] = '';
   	if (isset($page['items'])) {
   		$menu .= $getPage === $page['alias'] ? '<li class="dropdown active">' : '<li class="dropdown">'; 
   		$menu .= '<a class="dropdown-toggle" data-toggle="dropdown" href="#">';
   		$menu .= $page['title'] . '</a>';
   		$menu .= '<ul class="dropdown-menu">';
   		foreach ($page['items'] as $subPage) {
   			$menu .= $getPage === $subPage['alias']  ? '<li class="parent active">' : '<li>';
   			$menu .= '<a href="' . $path  . '/' . $page['alias'] . '/' . $subPage['alias'] . '">';
   			$menu .= $subPage['title'] . '</a></li>';
   		}
   		$menu .= '</ul>';
   	} else {		
   		$menu .= $getPage === $page['alias'] ? '<li class="active">' : '<li>';   
		if (isset($page['fileRender']) && $page['fileRender'] != null) {
		   	$menu .= '<a href="' . $page['fileRender'] . '" target="_blank">';	
		} else {
   			$menu .= '<a href="' . $path  . '/' . $page['alias'] . '">';			
		}
   		$menu .= $page['title'] . '</a></li>'; 		
   	}
}
$menu .= '</ul>';

/**
 * Carrega o cabeçalho.
 */     	
include('header.php');

/**
 * Carrega a página.
 * O arquivo de cada página deve estar localizado na pasta 'pages' e deve
 * coincidir com o alias da página. Caso o arquivo não seja localizado,
 * será carregada a página de erro.
 * Exceto no caso de estar especificado o 'fileRender', nesse caso, 
 * o usuário será redirecionado para a home.
 */    
if ((isset($Page->fileRender) && $Page->fileRender != null)) {
	header("Location: $path");
} else {
	$fileRender = $folder . '/pages/';
	$fileRender .= $Page->parent !== '' ? $Page->parent . '/' : '';
	$fileRender .= $Page->alias !== '' ? $Page->alias : '';
	$fileRender .= '.php';
	if (is_file($fileRender)) 
		include($fileRender);
	else
	   	include($folder . '/error404.php');
}

/**
 * Carrega o rodapé.
 */  
include('footer.php'); 
?>