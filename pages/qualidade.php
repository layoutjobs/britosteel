<section id="main" class="no-bg">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="br br5"></div>
				<h1 class="text-right">
				Todo o processo de<br />
				execução do projeto é<br />
				acompanhado de perto<br />
				por profissionais<br />
				treinados.</h1>
			</div>
			<div class="span6">
				<div style="margin-left: 30px; margin-right: 30px;">
					<p>A Brito Steel se dedica a cada dia em aprimorar sua qualidade através 
					de novos equipamentos, nova tecnologia e acima de tudo através da 
					capacitação dos seus colaboradores.</p>
					<div class="br"></div>
					
					<p>Qualidade é a palavra de ordem. Todo o processo de execução do projeto 
					é acompanhado de perto por profissionais treinados e aptos para organizar 
					de forma ágil, a construções do equipamento desde o desenho do projeto 
					até a entrega para o cliente.</p>
					<div class="br"></div>
					
					<p>Com isso a Brito Steel garante total qualidade e agilidade do início 
					ao fim do projeto, com o compromisso de concluir o desenvolvimento do 
					equipamento, sempre dentro dos prazos estipulados. Qualidade e 
					compromisso estão presentes no dia a dia da Brito Steel.</p>
				<div>
			</div>
		</div>
	</div>
</section><!-- /#main -->	