<?php 
	$isError = false;
	$isPost = false;

	$nome 		= isset($_POST['txtNome']) 		? $_POST['txtNome']		: '';
	$empresa 	= isset($_POST['txtEmpresa']) 	? $_POST['txtEmpresa'] 	: '';
	$email		= isset($_POST['txtEmail']) 	? $_POST['txtEmail'] 	: '';
	$telefone	= isset($_POST['txtTelefone']) 	? $_POST['txtTelefone'] : '';
	$mensagem	= isset($_POST['txtMensagem']) 	? $_POST['txtMensagem'] : '';

	if (isset($_POST['submit'])) {
		$isPost	= true;

		require_once $folder . '/lib/phpmailer/class.phpmailer.php';

		$mail = new PHPMailer();
		if ($mailIsSMTP) 	$mail->IsSMTP();
		$mail->Host			= $mailHost;
		$mail->Port       	= $mailPort;
		$mail->SMTPAuth   	= $mailSMTPAuth;
		$mail->SMTPSecure 	= $mailSMTPSecure;
		$mail->SMTPDebug  	= $mailSMTPDebug;
		$mail->Username   	= $mailUsername;
		$mail->Password   	= $mailPassword;
		$mail->CharSet      = $mailCharSet;	
		// 11/12/2012: 
		// Desativado devido a conflitos no servidor do cliente.
		// Alterado para um email interno.
		/* 
		$mail->From		  	= $email;
		$mail->FromName	  	= $nome;
		*/
		$mail->From		  	= $emailsContato[0];
		$mail->FromName	  	= $siteTitle;
		foreach($emailsContato as $value) {
			$mail->AddAddress($value, $siteTitle);				
		}
		$mail->AddReplyTo($email, $nome);

		$body = "<strong>Nome:</strong> $nome<br />";
		$body .= "<strong>Empresa:</strong> $empresa<br />";
		$body .= "<strong>E-mail:</strong> $email<br />";
		$body .= "<strong>Telefone:</strong> $telefone<br />";
		$body .= "<strong>Mensagem:</strong><br />$mensagem<br />";
		
		$mail->IsHTML(true);
		$mail->WordWrap   = 50;
		$mail->Subject    = "Website > Contato";
		$mail->Body       = $body;

		$isError = !$mail->Send();
	}	
?>
<div class="form" style="width: 280px;">
	<?php if ($isError && $isPost) { ?>
	<div class="alert alert-error">
		<strong class="alert-heading">Ops... </strong>
		Desculpe, ocorreu um erro ao tentar enviar sua mensagem. Verifique os campos e tente 
		novamente. Se o problema persistir, por favor, utilize outro dos nossos meios de contato.
	</div>
	<?php } else if ((!$isError) && $isPost) { ?>
	<div class="alert alert-success">
		<strong class="alert-heading">Sucesso! </strong>
		Sua mensagem foi enviada com sucesso. Obrigado por entrar em contato, retornaremos sua 
		mensagem o mais breve possível.
	</div>
	<?php } else { ?>
	<form id="form-contato" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
		<fieldset>
			<div class="controls">
				<label for="txtNome">Nome <span class="required-label">*</span></label>
				<input type="text" class="input-xlarge required" id="txtNome" name="txtNome" value="<?php echo $nome; ?>" />
			</div>
			
			<div class="controls">
				<label for="txtEmpresa">Empresa</label>
				<input type="text" class="input-xlarge" id="txtEmpresa" name="txtEmpresa" value="<?php echo $empresa; ?>" />
			</div>
			
			<div class="controls">
				<label for="txtEmail">E-mail <span class="required-label">*</span></label>
				<input type="text" class="input-xlarge required email" id="txtEmail" name="txtEmail" value="<?php echo $email; ?>" />
			</div>
			
			<div class="controls">
				<label for="txtTelefone">Telefone</label>
				<input type="text" class="input-xlarge" id="txtTelefone" name="txtTelefone" value="<?php echo $telefone; ?>" />
			</div> 
			
			<div class="controls">
				<label for="txtMensagem">Mensagem <span class="required-label">*</span></label>
				<textarea class="input-xlarge required" id="txtMensagem" rows="2" name="txtMensagem"><?php echo $mensagem; ?></textarea>
			</div>       
		</fieldset>          
		<button type="submit" name="submit" class="btn btn-primary">Enviar</button>
	</form>
	<?php }; ?>
</div><!-- /.form -->

<script src="<?php echo $jsPath; ?>jquery.validate.min.js"></script>
<script>
	$(document).ready(function(){
		$.extend(jQuery.validator.messages, {
		    required: 'Obrigatório.',
		    email: 'E-mail inválido.',
		});
		$('#form-contato').validate({
			errorElement: 'span',
	        errorPlacement: function(error, element) {           
	            error.insertAfter(element);
	            error.addClass('label label-important'); 
	            $(element).addClass('error');
	        },
		});
	});
</script>