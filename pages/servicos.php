<section id="main">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="br br5"></div>
				
				<h1 class="text-right">
				Para outros<br />
				equipamentos ou<br />	
				automatização,<br />
				entre em contato.</h1>
				<div class="br br5"></div>
				<div class="br br5"></div>
				<div class="br br5"></div>
				
				<p class="text-center">Equipamentos desenvolvidos pela Brito Steel, 
				clique para ampliar.</p>
				
				<section id="portfolio" class="gallery carousel slide">
					<div class="carousel-inner">
						<div class="item active">
							<a href="<?php echo $uploadsPath; ?>portfolio/equipamento-1.jpg" rel="prettyPhoto[gallery]">
								<img src="<?php echo $uploadsPath; ?>portfolio/equipamento-1.jpg" alt="Equipamentos desenvolvidos pela Brito Steel." />
							</a>	
							<div class="carousel-caption">
	                  			<p class="text-right"><strong>Equipamentos desenvolvidos pela Brito Steel.</strong></p>
	                		</div>					
						</div>
						<div class="item">
							<a href="<?php echo $uploadsPath; ?>portfolio/equipamento-2.jpg" rel="prettyPhoto[gallery]">
								<img src="<?php echo $uploadsPath; ?>portfolio/equipamento-2.jpg" alt="Equipamentos desenvolvidos pela Brito Steel." />
							</a>
							<div class="carousel-caption">
	                  			<p class="text-right"><strong>Equipamentos desenvolvidos pela Brito Steel.</strong></p>
	                		</div>						
						</div>
						<div class="item">
							<a href="<?php echo $uploadsPath; ?>portfolio/equipamento-3.jpg" rel="prettyPhoto[gallery]">
								<img src="<?php echo $uploadsPath; ?>portfolio/equipamento-3.jpg" alt="Equipamentos desenvolvidos pela Brito Steel." />
							</a>
							<div class="carousel-caption">
	                  			<p class="text-right"><strong>Equipamentos desenvolvidos pela Brito Steel.</strong></p>
	                		</div>						
						</div>
						<div class="item">
							<a href="<?php echo $uploadsPath; ?>portfolio/equipamento-4.jpg" rel="prettyPhoto[gallery]">
								<img src="<?php echo $uploadsPath; ?>portfolio/equipamento-4.jpg" alt="Equipamentos desenvolvidos pela Brito Steel." />
							</a>
							<div class="carousel-caption">
	                  			<p class="text-right"><strong>Equipamentos desenvolvidos pela Brito Steel.</strong></p>
	                		</div>							
						</div>
					</div>
					<a class="carousel-control left" href="#portfolio" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#portfolio" data-slide="next">&rsaquo;</a>
				    <div class="carousel-pagination">
				        <a href="#portfolio" class="pagination-active">0</a>
				        <a href="#portfolio">1</a>
				        <a href="#portfolio">2</a>
				        <a href="#portfolio">2</a>
				    </div>
				</section>					
					
			</div>
			<div class="span6">
				<div style="margin-left: 30px; margin-right: 50px;">
					<p><strong>Stress teste:</strong><br /> 
					Sistema que simula as vibrações que sobre o cluster com o carro em 
					movimento, testando assim durabilidade dos encaixes dos ponteiros, 
					lentes, etc.</p>
					<div class="br"></div>
		
					<p><strong>Dispositivo de estanqueidade:</strong><br /> 
					Este dispositivo é usado para fazer o teste final de vazão de ar do 
					carro borboleta.</p>
					<div class="br"></div>
	
					<p><strong>Prensa pneumática:</strong><br /> 
					Está máquina é usada para inserção do clipeser no bico injetor.</p>
					<div class="br"></div>
	
					<p><strong>Painel elétrico:</strong><br />
					Painel usado na automatização das máquinas fornecidas pela Brito Steel.</p>
					<div class="br"></div>
	
					<p><strong>Parafusamento do bico injetor:</strong><br />
					Está faz o parafusamento do bico injetor e lubrificação.</p>
					<div class="br"></div>
	
					<p><strong>Dispositivo Elevador:</strong><br />
					Dispositivo usado para alimentação de peças Alice do ar condicionado.</p>
					<div class="br"></div>
	
					<p><strong>Painel Pneumático:</strong><br />
					Painel usado na automatização das máquinas fornecidas da Brito s Steel.</p>
					<div class="br"></div>
	
					<p><strong>Maquina de Engraxar:</strong><br /> 
					Máquina usada para engraxar automaticamente o motor de partida.</p>
					<div class="br"></div>
	
					<p><strong>Máquina de cravamento:</strong><br />
					Máquina usada para inserir rolamento na carcaça alternador, nesta 
					máquina também contém um posto de parafusamento do alternador.</p>
					<div class="br"></div>
	
					<p><strong>Máquina de Fechamento do Cluster:</strong><br />
					Equipamentos automático para clipsagem da lente na moldura, verificação 
					automática de 100% da operação e fechamento  do produto de torque e 
					verificação do número de parafusos a serem utilizados.</p>
					<div class="br"></div>
	
					<p><strong>Máquina de cravamento:</strong><br />
					Máquina usada para cravamento do pino motor do corpo de borboleta, 
					medindo força de inserção e curso de posicionamento.</p>
					<div class="br"></div>
	
					<p><strong>Teste Medidor Tubolar: </strong><br />
					Máquina usada para medir a movimentação da bóia resistência e funcional.</p>
					<div class="br br2"></div>
					
					<a href="<?php echo $uploadsPath; ?>servicos_britosteel.pdf" target="_blank">
						<h3 class="text-upper">Imprimir Lista de serviços</h3>
					</a>
					<div class="br"></div>
				</div>
			</div>
		</div>
	</div>
</section><!-- /#main -->	