<section id="main">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="br br4"></div>
				<h1 class="text-right">
				Há 10 anos a<br />
				Brito Steel cria e<br />
				desenvolve solução<br />
				em automação<br />
				industrial.</h1>
			</div>
			<div class="span6">
				<div style="margin-left: 30px; margin-right: 30px;">
					<p style="line-height: 24px;">Para informações, dúvidas ou orçamentos, entre em contato:<br />
					<strong class="text-primary" style="font-size: 18px;">
					19 3272.4747 ou 19 3272.5892<br />
					<a href="mailto:contato@britosteel.com.br">contato@britosteel.com.br</a></strong></p>
					<p>Ou preencha o formulário abaixo, que <br />breve entraremos em contato.</p>
					<div class="br"></div>
					
					<?php include_once '_form-contato.php' ?>
				<div>
			</div>
		</div>
	</div>
</section><!-- /#main -->