<section id="main">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="br br5"></div>
				<h1 class="text-right">
				Há 10 anos a<br />
				Brito Steel cria e<br />
				desenvolve solução<br />
				em automação<br />
				industrial.</h1>
			</div>
			<div class="span6">
				<div style="margin-left: 30px; margin-right: 40px;">
					<p>Através da solicitação do cliente, a Brito Steel oferece a melhor solução 
					custo x benefício para cada necessidade específica. São projetos, máquinas e 
					equipamentos desenvolvidos com a exclusividade e a personalização que cada 
					empresa necessita.</p>
					<div class="br"></div>
						
					<p>Com isso, a Brito Steel além de oferecer um serviço de qualidade e 
					personalizado, também orienta o cliente a criar um equipamento utilizando 
					somente o recurso necessário, para que a automação seja realizada de forma 
					eficiente e satisfatória.</p>
					<div class="br"></div>
					
					<p>A Brito Steel trabalha com um conceito chamado "solução", e isso é um grande 
					diferencial quando o assunto é automação industrial. Solicite uma apresentação
					sem compromisso.</p>
				</div>
			</div>
		</div>
	</div>
</section><!-- /#main -->	