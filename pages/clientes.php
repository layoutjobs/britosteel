<section id="main">
	<div class="container">
		<div class="row">
			<div class="span5">
				<div class="br br4"></div>
				<h1 class="text-right">
				Principais<br />
				clientes<br />
				Brito Steel</h1>
			</div>
			<div class="span7">
				<div style="margin-left: 20px;">
					<ul class="clientes">
						<li><img src="<?php echo $uploadsPath; ?>clientes/continental.jpg" alt="Continental" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/brose.jpg" alt="Brose" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/valeo.jpg" alt="Valeo Service" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/saf.jpg" alt="SAF Holland" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/johnson-controls.jpg" alt="Jhonson Controls" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/colsol.jpg" alt="Colsol Aquecedores" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/sulbras.jpg" alt="Sulbras" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/cunzolo.jpg" alt="Cunzolo" /></li>
						<li><img src="<?php echo $uploadsPath; ?>clientes/saint-gobain.jpg" alt="Saint-Gobain" /></li>
					</ul>
				<div>
			</div>
		</div>
	</div>
</section><!-- /#main -->	