<section id="main" class="no-bg">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="br br4"></div>
				<h1 class="text-right">
				A Brito Steel está<br />
				estratégicamente<br />
				localizada, próxima as<br />
				principais rodovias.</h1>
			</div>
			<div class="span6">
				<div style="margin-left: 30px; margin-right: 25px;">
					<p>A Brito Steel está localizada em Campinas, próxima as principais 
					rodovias do estado de São Paulo. Facilitando a logística e agilizando a
					entrega dos equipamentos para todo o estado. </p>
					<div class="br"></div>
					
					<div id="mapa" class="loading" style="width: 405px; height: 220px;"></div>	
				<div>
			</div>
		</div>
	</div>
</section><!-- /#main -->

<script src="https://maps.google.com/maps/api/js?sensor=true"></script>
<script>
	function initialize(){
		var myOptions = {
	       	zoom: <?php echo $mapaZoom; ?>,
			center: new google.maps.LatLng(<?php echo $mapaLat; ?>, <?php echo $mapaLng; ?>),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById('mapa'), myOptions);
		var marker = new google.maps.Marker({
			position: map.getCenter(),
			map: map,
			title: '<?php echo $siteTitle; ?>'
        });
		google.maps.event.addListener(map, 'center_changed', function(){
			window.setTimeout(function(){
				map.panTo(marker.getPosition());
			}, 3000);
        });
		google.maps.event.addListener(marker,'click',function(){
			map.setZoom(16);
			map.setCenter(marker.getPosition());
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>