<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />
    <title><?php echo $Page->title . ' | ' . $siteTitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="description" content="Somos especialistas em Máquinas, Ferramentas, Acessórios e Dispositivos para Automação Industrial." />
    <meta name="author" content="Layout | Design e Publicidade" />

    <link href="<?php echo $cssPath; ?>main.css" rel="stylesheet" />
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo $jsPath; ?>jquery.js"></script>	
</head>
<body class="<?php echo ($Page->parent !== '' ? 'parent-page-' . $Page->parent . ' ' : '') . 'page-' . $Page->alias ; ?>">
	<div class="container"><div class="container"><div class="container">
		<header id="header">
			<a class="logo" href="<?php echo $path; ?>" title="<?php echo $siteTitle; ?>">
				<img src="<?php echo $imgPath; ?>logo.png" alt="<?php echo $siteTitle; ?>" />
			</a>
		</header>
		
		<?php if(isset($Page->banners)) : ?>

		<section id="banners" class="carousel">
			<div class="carousel-inner">			

				<?php foreach ($Page->banners as $key=>$value) : ?>
					<div <?php echo $key === 1 ? 'class="item active"' : 'class="item"'; ?>>
						<img src="<?php echo $uploadsPath; ?>banners/banner-<?php echo $value; ?>.jpg" />
					</div>
				<?php endforeach; ?>
		
			</div>
		</section>	
	
		<?php endif; ?>
		
		<nav id="mainmenu">
			<div class="container">
				<?php echo $menu; ?>	
			</div>
		</nav>
