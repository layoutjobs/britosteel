		<footer id="footer">
			<p>Copyright &copy; <?php echo date('Y'); ?> <?php echo $siteTitle; ?>. 
			Todos os direitos reservados. <strong>Contato: (19) 3272.4747 - (19) 3272.5892</strong><br />
			Rua Edmundo Navarro de Andrade, 2104 VL. Anhanguera - Cep: 13031-769 - Campinas SP</p><br />
			<a class="logo" href="<?php echo $path; ?>" title="<?php echo $siteTitle; ?>">
				<img src="<?php echo $imgPath; ?>footer-logo.png" alt="<?php echo $siteTitle; ?>" />
			</a>	
		</footer>
	</div></div></div><!-- /.containers -->

<script src="<?php echo $jsPath; ?>main.js"></script>	
<script src="<?php echo $jsPath; ?>bootstrap-carousel.js"></script>	
<script src="<?php echo $jsPath; ?>bootstrap-transition.js"></script>

<?php if (in_array('prettyphoto', $Page->plugins)) : ?>
	<script src="<?php echo $pluginsPath; ?>prettyphoto/js/jquery.prettyPhoto.js"></script>
	<link rel="stylesheet" href="<?php echo $pluginsPath; ?>prettyphoto/css/prettyPhoto.css" />
	<script>
		$(document).ready(function(){
			$('a[rel^="prettyPhoto"]').prettyPhoto();
		});
	</script>
<?php endif; ?>

</body>
</html>